import axios from '@/utilities/axios';
import { AxiosResponse } from 'axios';
import { Lesson } from '@/types/lesson';
import { LessonsResponse } from '@/pages/Admin/store/types';
import { FiltrerAndPagingParams, parsedRequestParams } from '@/filter';
import { LessonResponse } from '@/pages/User/store/types';

export default class AdminLessonApi {
  public static async getLessons(params?: { filters: any }): Promise<AxiosResponse<LessonsResponse>> {
    return axios.get('/lessons');
  }
  public static async getLessonsByFilters(params: FiltrerAndPagingParams): Promise<AxiosResponse<LessonResponse>> {
    return axios.get(`/lessons/${parsedRequestParams(params)}`);
  }
  public static async getFavoriteLessons(params?: { filters: any }): Promise<AxiosResponse<LessonsResponse>> {
    return axios.get('/user/favorite-lessons');
  }
  public static async addFavorite(id: number) {
    return axios.post('/user/favorite-lessons', { lessonId: id });
  }
  public static async deleteFavorite(id: number) {
    return axios.delete(`/user/favorite-lessons/`, { data: { lessonId: id } });
  }
  public static addLesson(params: Omit<Lesson, 'id'>) {
    return axios.post('/lessons', params);
  }
  public static editLesson(id: number, data: Lesson) {
    return axios.patch(`/lessons/${id}`, data);
  }
  public static deleteLesson(id: number) {
    return axios.delete(`/lessons/${id}`);
  }
  public static getFirstFreeLesson() {
    return axios.get(`/lessons?paging[page]=1&paging[limit]=1&filter[price][value]=0`);
  }
  public static getFavorites() {
    return axios.get(`/lessons?paging[page]=1&paging[limit]=1&filter[price][operator]=equial&filter[price][value]=0`);
  }
  public static getLessonByID(id: number) {
    return axios.get(`/lessons/${id}`);
  }
}
