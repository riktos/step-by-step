import { EditProfileData } from '@/types/auth';
import axios from '@/utilities/axios';
import { AxiosResponse } from 'axios';

export default class AuthApi {
  public static async authorize(data: any): Promise<AxiosResponse<any>> {
    return axios.post('/login', data);
  }
  public static async register(data: any): Promise<AxiosResponse<any>> {
    return axios.post('/register', data);
  }

  public static async edit(data: EditProfileData): Promise<AxiosResponse<any>> {
    return axios.post('/user-update', data);
  }
  public static async sendToEmail(data: any) {
    return axios.post('/recovery', {
      email: data,
    });
  }
  public static async restorePass(data: {
    passwordRecoveryToken: string; // 'Breare <token>'
    password: string;
  }): Promise<AxiosResponse<any>> {
    return axios.post('/recovery-password', data);
  }
}
