import { Component, Vue } from 'vue-property-decorator';
import Header from '@/components/Header';
import SidebarLeft from '@/components/SidebarLeft';
import SidebarRight from '@/components/SidebarRight';

@Component({
  components: {
    Header,
    SidebarLeft,
  },
})
export default class DefaultLayout extends Vue {

}
