import { Component, Vue } from 'vue-property-decorator';
import Header from '@/components/Header';
import SidebarLeft from '@/components/SidebarLeft';
import SidebarRight from '@/components/SidebarRight';

@Component({
  components: {
    Header,
    SidebarLeft,
    SidebarRight,
  },
})
export default class UserLayout extends Vue {

}
