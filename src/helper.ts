export function debounce<F extends (...params: any[]) => void>(fn: F, delay: number) {
  let timeoutID: number = 500;
  return function(this: any, ...args: any[]) {
    clearTimeout(timeoutID);
    timeoutID = window.setTimeout(() => fn.apply(this, args), delay);
  } as F;
}
export function uniq(value: any, index: number, self: any): any {
  return self.indexOf(value) === index;
}
