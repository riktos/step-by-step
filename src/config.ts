// tslint:disable-next-line
const webCliVersion = require('../package.json').version;

const config = (window as any).config as Config;
const env = process.env;

interface Config {
  apiUrl: string;
  env: string;
  pageSize: number;
  idleTime: number;
}
// }
// if (env.VUE_APP_API_URL) {
//   config.apiUrl = env.VUE_APP_API_URL;
// }

// if (env.NODE_ENV) {
//   config.env = env.NODE_ENV;
// }

export default config;
