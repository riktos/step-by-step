import { Component, Prop, Vue, Watch } from 'vue-property-decorator';
import CustomButton from '@/components/Button';
import CustomInput from '@/components/Input';
import AdminLessonApi from '@/api/AdminLessons';
import { Lesson } from '@/types/lesson';
import { namespace } from 'vuex-class';
import { Validations } from 'vuelidate-property-decorators';
import { required, minLength, maxLength, minValue, url, numeric } from 'vuelidate/lib/validators';
const adminLesson = namespace('adminLesson');
import CustomSelect from '@/components/CustomSelect';

const defaultData = {
  author: '',
  category: 'pro',
  description: '',
  fullDescription: '',
  instagram: '',
  name: '',
  price: 0,
  telegram: '',
  youtube: '',
  youtubeCode: '',
};

@Component({
  components: {
    CustomButton,
    CustomInput,
    CustomSelect,
  },
})
export default class LessonPage extends Vue {
  get goButtonText() {
    if (this.$route.params.id) {
      return 'Редактировать урок';
    }
    return 'Добавить урок';
  }

  get titleText() {
    if (this.$route.params.id) {
      return 'Редактирование урока';
    }
    return 'Добавление урока';
  }
  private placeholder = `Например: \n\n<iframe width="560" height="315" src="https://www.youtube.com/embed/arWBReeLBgw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`;
  @adminLesson.State private lessons!: Lesson[];
  @adminLesson.State private lesson!: Lesson | null;
  @adminLesson.Action private editLesson!: (payload: { id: number; data: Lesson }) => Promise<void>;
  @adminLesson.Action private addLesson!: (data: Omit<Lesson, 'id'>) => Promise<void>;
  @adminLesson.Action private getLessonByID!: (id: number) => Promise<void>;

  private loading: boolean = false;
  private error: boolean = false;
  private submitted: boolean = false;
  private errorForm: boolean = false;
  private lessonData: any = {
    author: '',
    category: 'beginner',
    description: '',
    fullDescription: '',
    instagram: '',
    name: '',
    price: 0,
    telegram: '',
    youtube: '',
    youtubeCode: '',
  };

  private options = [
    { name: 'Начальный', key: 'beginner', active: false },
    { name: 'Продвинутый', key: 'pro', active: false },
  ];

  @Validations()
  private validations = {
    lessonData: {
      author: { required, minLength: minLength(6) },
      category: { required },
      description: { required, minLength: minLength(6) },
      fullDescription: { required, minLength: minLength(6), maxLength: maxLength(10000) },
      instagram: { url },
      telegram: { url },
      name: { required, minLength: minLength(6) },
      price: { required, minValue: minValue(0), numeric },
      youtube: { required, url },
      youtubeCode: { required, url },
    },
  };
  private changeCategory(value: string) {
    this.lessonData.category = value;
  }
  private async created() {
    this.loading = true;
    this.lessonData = Object.assign({}, defaultData);

    const isLessonID = !isNaN(parseInt(this.$route.params.id, 10));
    try {
      if (isLessonID) {
        await this.getLessonByID(parseInt(this.$route.params.id, 10));
      }
    } finally {
      if (isLessonID) {
        this.lessonData = Object.assign({}, this.lesson);
      }

      this.loading = false;
    }
  }

  private async onGo() {
    this.submitted = true;
    this.$v.$touch();
    if (this.$v.$invalid) {
      this.$v.$reset();
      return;
    }
    this.loading = true;
    if (!this.$route.params.id) {
      try {
        this.loading = false;
        await this.addLesson(this.lessonData);
        this.onBack();
      } catch (error) {
        this.error = true;
        this.loading = false;
      }
    } else {
      try {
        await this.editLesson({ id: parseInt(this.$route.params.id, 10), data: this.lessonData });
        this.submitted = true;
        this.loading = false;
        this.$router.push({ name: 'admin' });
      } catch (error) {
        this.error = true;
        this.loading = false;
      }
    }
  }
  private onBack() {
    this.$router.go(-1);
  }

  private authorErrorHandler() {
    if (!this.$v.lessonData.author!.required) {
      return 'Обязательное поле';
    } else if (!this.$v.lessonData.author!.minLength) {
      return 'Не мение 6 символов';
    }
  }
  private descriptionErrorHandler() {
    if (!this.$v.lessonData.description!.required) {
      return 'Обязательное поле';
    } else if (!this.$v.lessonData.description!.minLength) {
      return 'Не мение 6 символов';
    }
  }
  private nameErrorHandler() {
    if (!this.$v.lessonData.name!.required) {
      return 'Обязательное поле';
    } else if (!this.$v.lessonData.name!.minLength) {
      return 'Не мение 6 символов';
    }
  }
  private fullDescriptionErrorHandler() {
    if (!this.$v.lessonData.fullDescription!.required) {
      return 'Обязательное поле';
    } else if (!this.$v.lessonData.fullDescription!.minLength) {
      return 'Не мение 6 символов';
    } else if (!this.$v.lessonData.fullDescription!.maxLength) {
      return 'Не более 10к символов';
    }
  }
  private instagramErrorHandler() {
    if (!this.$v.lessonData.instagram!.url) {
      return 'Значение не является ссылкой';
    }
  }
  private telegramErrorHandler() {
    if (!this.$v.lessonData.telegram!.url) {
      return 'Значение не является ссылкой';
    }
  }
  private youtubeErrorHandler() {
    if (!this.$v.lessonData.youtube!.required) {
      return 'Обязательное поле';
    } else if (!this.$v.lessonData.youtube!.url) {
      return 'Значение не является ссылкой';
    }
  }
  private youtubeCodeErrorHandler() {
    if (!this.$v.lessonData.youtubeCode!.required) {
      return 'Обязательное поле';
    } else if (!this.$v.lessonData.youtubeCode!.url) {
      return 'Значение не является ссылкой';
    }
  }
  private priceErrorHandler() {
    if (!this.$v.lessonData.price!.required) {
      return 'Обязательное поле';
    } else if (!this.$v.lessonData.price!.minValue) {
      return 'Минимальная цена урока 0';
    } else if (!this.$v.lessonData.price!.numeric) {
      return 'Введите цифровое значаение';
    }
  }
}
