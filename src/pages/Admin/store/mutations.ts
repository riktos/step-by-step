import { LessonResponse } from '@/pages/User/store/types';
import { Lesson } from '@/types/lesson';
import Vue from 'vue';
import { MutationTree } from 'vuex';
import { ADD_LESSON, DELETE_LESSON, EDIT_LESSON, SET_LESSONS, SET_LESSON_BY_ID } from './mutationTypes';
import { AdminLessonState } from './types';

export const mutations: MutationTree<AdminLessonState> = {
  [SET_LESSONS](state, data: LessonResponse) {
    const { meta, data: Lessons } = data;

    state.total = meta.total;
    state.lastPage = meta.last_page;
    state.lessons = Lessons;
  },
  [ADD_LESSON](state, payload: Lesson) {
    state.lessons.push(payload);
  },
  [EDIT_LESSON](state, payload: Lesson) {
    const index = state.lessons.findIndex(item => payload.id === item.id);
    if (index !== -1) {
      state.lessons.splice(index, 1, payload);
    }
  },
  [DELETE_LESSON](state, id: number) {
    const index = state.lessons.findIndex(item => id === item.id);
    if (index !== -1) {
      state.lessons.splice(index, 1);
    }
  },
  [SET_LESSON_BY_ID](state, item: Lesson) {
    state.lesson = item;
  },
};
