import { RootState } from '@/store/types';
import Vue from 'vue';
import Vuex, { Module, Store } from 'vuex';
import { actions } from './actions';
import { mutations } from './mutations';
import { AdminLessonState } from './types';

Vue.use(Vuex);
export const state: AdminLessonState = {
  lessons: [],
  loading: false,
  error: null,
  filters: {
    page: 1,
  },
  total: 0,
  lastPage: 0,
  lesson: null,
};

const namespaced: boolean = true;

export const adminLesson: Module<AdminLessonState, RootState> = {
  namespaced,
  state,
  actions,
  mutations,
};
