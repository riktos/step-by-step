import { FiltrerAndPagingParams } from '@/filter';
import { Lesson } from '@/types/lesson';

export interface AdminLessonState {
  lessons: Lesson[];
  loading: boolean;
  error: any;
  filters: FiltrerAndPagingParams;
  lastPage: number;
  total: number;
  lesson: Lesson | null;
}

export interface LessonsResponse {
  data: Lesson[];
  meta: any;
}
