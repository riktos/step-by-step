export const SET_LESSONS = 'SET_LESSONS';
export const ADD_LESSON = 'ADD_LESSON';
export const EDIT_LESSON = 'EDIT_LESSON';
export const DELETE_LESSON = 'DELETE_LESSON';
export const LOADING = 'LOADING';
export const ERROR = 'ERROR';
export const SET_LESSON_BY_ID = 'SET_LESSON_BY_ID';
