import AdminLessonApi from '@/api/AdminLessons';
import { FiltrerAndPagingParams } from '@/filter';
import { RootState } from '@/store/types';
import { Lesson } from '@/types/lesson';
import { ActionTree } from 'vuex';
import { DELETE_LESSON, ERROR, LOADING, SET_LESSONS, SET_LESSON_BY_ID } from './mutationTypes';

import { AdminLessonState } from './types';

export const actions: ActionTree<AdminLessonState, RootState> = {
  async getLessons({ state, commit }) {
    state.lessons = [];
    if (state.filters.page > state.lastPage && state.lastPage !== 0) {
      return;
    }
    try {
      const params: FiltrerAndPagingParams = {
        filters: state.filters.filters,
        page: state.filters.page,
        search: state.filters.search,
      };
      const { data } = await AdminLessonApi.getLessonsByFilters(params);
      // dispatch('setPage', state.filters.page++);
      commit(SET_LESSONS, data);
    } catch (err) {
      console.error(err);
    }
  },
  async addLesson({ commit }, lessonData: Lesson) {
    try {
      const { data } = await AdminLessonApi.addLesson(lessonData);
      commit(SET_LESSONS, data);
    } catch (error) {
      commit(ERROR, error);
    }
  },

  async editLesson({ commit }, { id, data }: { id: number; data: Lesson }) {
    try {
      const newData = Object.assign({}, data);
      await AdminLessonApi.editLesson(id, newData);
    } catch (error) {
      commit(ERROR, error);
    }
  },
  async deleteLesson({ commit }, id: number) {
    commit(LOADING, true);
    try {
      await AdminLessonApi.deleteLesson(id);
      commit(DELETE_LESSON, id);
    } catch (error) {
      commit(ERROR, error);
    }
  },
  setFilters({ commit, state }, data: Array<Record<string, string | number>>) {
    state.filters.filters = data;
  },
  setSearch({ commit, state }, value: string) {
    state.filters.search = value;
  },
  setPage({ commit, state }, value: number) {
    state.filters.page = value;
  },
  clearLessons({ state }) {
    state.filters.page = 1;
    state.lastPage = 0;
    state.lessons = [];
  },
  async getLessonByID({ commit }, id: number) {
    try {
      let data;

      data = (await AdminLessonApi.getLessonByID(id)).data;

      commit(SET_LESSON_BY_ID, data);
    } catch (error) {
      commit(ERROR, error);
    }
  },
};
