import { Component, Vue, Watch } from 'vue-property-decorator';
import CustomButton from '@/components/Button';
import CustomInput from '@/components/Input';
import LessonRow from '@/components/LessonRow';
import CustomSelect from '@/components/CustomSelect';
import Loader from '@/components/Loader';
import BasePagination from '@/components/Paggin';
import { namespace } from 'vuex-class';
import { Lesson } from '@/types/lesson';
import { FiltrerAndPagingParams } from '@/filter';

const adminLesson = namespace('adminLesson');

@Component({
  components: {
    CustomButton,
    CustomInput,
    LessonRow,
    CustomSelect,
    BasePagination,
    Loader,
  },
})
export default class LoginPage extends Vue {
  private get page() {
    return this.filters.page;
  }
  private get pageCount() {
    return Math.ceil(this.total / 6);
  }

  private foundQuery: string = '';
  @adminLesson.State private lessons!: Lesson[];
  @adminLesson.State private filters!: FiltrerAndPagingParams;
  @adminLesson.State private total!: number;

  @adminLesson.Action private getLessons!: () => Promise<void>;
  @adminLesson.Action private setFilters!: (data: Array<Record<string, string | number>>) => void;
  @adminLesson.Action private setPage!: (value: number) => void;
  @adminLesson.Action private setSearch!: (value: string) => void;
  @adminLesson.Action private clearLessons!: () => void;

  private componentFilters: Array<Record<string, string | number>> = [];
  private loading: boolean = false;
  private debounce: any = null;

  private options = [
    { name: 'Все', key: 'none', active: true },
    { name: 'Начальный', key: 'beginner', active: false },
    { name: 'Продвинутый', key: 'pro', active: false },
  ];
  private category: string = this.options[0].key;

  private get filtersOnPage() {
    return this.filters.filters;
  }
  private get visiblePagesCount() {
    return this.total <= 6 ? 1 : 5;
  }

  public async pageChangeHandle(value: string) {
    switch (value) {
      case 'next':
        this.setPage(this.page + 1);
        break;
      case 'previous':
        this.setPage(this.page - 1);
        break;
      default:
        this.setPage(parseInt(value, 10));
        break;
    }
    await this.getLessons();
  }

  private changeCategory(value: string) {
    this.category = value;
    const categoryFilter = { category: this.category };
    let filtersForRequest: Array<Record<string, any>> = [];
    if (this.category !== 'none') {
      filtersForRequest = [categoryFilter];

      this.setFilters(filtersForRequest);
    } else {
      filtersForRequest = [];
      this.setFilters(filtersForRequest);
    }
  }

  private async created() {
    await this.$nextTick();
    this.loading = true;
    try {
      await this.getLessons();
    } finally {
      this.loading = false;
    }
  }

  @Watch('category')
  @Watch('foundQuery')
  private async onFilters() {
    this.clearLessons();
    this.setPage(1);
    this.loading = true;
    try {
      await this.getLessons();
    } finally {
      this.loading = false;
    }
  }

  private async loadNext(page: number) {
    this.loading = true;
    try {
      this.setPage(page);

      await this.getLessons();
    } finally {
      this.loading = false;
    }
  }

  private setFoundQuery(value: string) {
    clearTimeout(this.debounce);
    this.debounce = setTimeout(() => {
      this.foundQuery = value;
      this.setSearch(this.foundQuery);
    }, 600);
  }

  private addNewLesson(): void {
    this.$router.push({
      name: 'create',
      path: '/create',
    });
  }
}
