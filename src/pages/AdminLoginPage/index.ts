import { Component, Vue } from 'vue-property-decorator';
import CustomInput from '@/components/Input';
import CustomButton from '@/components/Button';
import AuthApi from '@/api/auth';
import { authorizationKey, authorizationKeyUser } from '@/constants/sessionStorageKeys';
import { remove, set } from '@/utilities/sessionStorage';
import { Validations } from 'vuelidate-property-decorators';
import { email, required } from 'vuelidate/lib/validators';

@Component({
  components: {
    CustomInput,
    CustomButton,
  },
})
export default class AdminLoginPage extends Vue {
  private loginData = {
    email: '',
    password: '',
  };
  @Validations()
  private validations = {
    loginData: {
      email: { required, email },
      password: { required },
    },
  };

  private error: boolean = false;
  private submited: boolean = false;
  private changeValue(field: string, value: string) {
    Vue.set(this.loginData, field, value);
  }
  private async onLogin() {
    this.$v.$touch();
    if (this.$v.$invalid) {
      this.submited = true;
      this.$v.$reset();
      return;
    }
    try {
      const { data, status } = await AuthApi.authorize({
        email: this.loginData.email,
        password: this.loginData.password,
      });

      if (status === 200) {
        set(authorizationKey, data.token);
        remove(authorizationKeyUser);
      }
      this.$router.push({
        name: 'admin',
      });
    } catch (error) {
      this.error = true;
    }
  }
  private emailValidation() {
    if (this.submited) {
      if (!this.$v.loginData.email!.required) {
        return 'Обязательное поле';
      }
      if (!this.$v.loginData.email!.email) {
        return 'Email не валидный';
      }
    }
  }
}
