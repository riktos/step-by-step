import { Component, Vue, Watch } from 'vue-property-decorator';
import CustomButton from '@/components/Button';
import CustomInput from '@/components/Input';
import Loader from '@/components/Loader';
import { namespace } from 'vuex-class';
import { Lesson } from '@/types/lesson';
import { YouTubeGetID } from '@/utilities/string';
const userLesson = namespace('userLesson');
@Component({
  components: {
    CustomButton,
    CustomInput,
    Loader,
  },
})
export default class UserPage extends Vue {
  private loading: boolean = false;
  private error: boolean = false;

  @userLesson.State private lessonByID!: Lesson | null;

  @userLesson.Action private getFavoriteLessons!: () => Promise<void>;
  @userLesson.Action private getLessonByID!: (id: number) => Promise<void>;
  @userLesson.Action private addFavorite!: () => Promise<void>;
  @userLesson.Action private deleteFavorite!: () => Promise<void>;
  @userLesson.Action private setFavorite!: (value: boolean) => void;

  private get isFavorite(): boolean {
    return this.lessonByID!.isFavorite!;
    // return false;
  }

  private async created() {
    this.loading = true;
    try {
      const lessonId = parseInt(this.$route.params.id, 10);
      await this.getLessonByID(lessonId);
    } catch (error) {
      this.error = true;
    } finally {
      this.loading = false;
    }
  }

  private async changeLocation(value: string) {
    window.location.href = value;
  }
  private get link(): string {
    const youtubeID = YouTubeGetID(this.lessonByID!.youtubeCode);
    return `https://www.youtube.com/embed/${youtubeID}?showinfo=0`;
  }
  private async onFavoriteClick() {
    const lessonId = parseInt(this.$route.params.id, 10);
    try {
      if (!this.isFavorite) {
        await this.addFavorite();
        this.setFavorite(true);
      } else {
        await this.deleteFavorite();
        this.setFavorite(false);
      }
    } catch (error) {
      console.error(error);
    }
  }

  @Watch('$route')
  private async onRouterChange() {
    this.loading = true;
    try {
      const lessonId = parseInt(this.$route.params.id, 10);
      await this.getLessonByID(lessonId);
    } catch (error) {
      this.error = true;
    } finally {
      this.loading = false;
    }
  }

  private get buttonText() {
    if (this.isFavorite) {
      return 'Удалить из избранного';
    } else {
      return 'Добавить в избранное';
    }
  }

  private onPay() {
    const widget = new cp.CloudPayments();
    widget.pay(
      'auth', // или 'charge'
      {
        // options
        publicId: 'test_api_00000000000000000000001', // id из личного кабинета
        description: 'Оплата товаров в example.com', // назначение
        amount: 100, // сумма
        currency: 'RUB', // валюта
        accountId: 'user@example.com', // идентификатор плательщика (необязательно)
        invoiceId: '1234567', // номер заказа  (необязательно)
        email: 'user@example.com', // email плательщика (необязательно)
        skin: 'mini', // дизайн виджета (необязательно)
        data: {
          myProp: 'myProp value',
        },
      },
      {
        onSuccess(options: any) {
          // success
          // действие при успешной оплате
        },
        onFail(reason: any, options: any) {
          // fail
          // действие при неуспешной оплате
        },
        onComplete(paymentResult: any, options: any) {
          // Вызывается как только виджет получает от api.cloudpayments ответ с результатом транзакции.
          // например вызов вашей аналитики Facebook Pixel
        },
      },
    );
  }
}
