import { Lesson } from '@/types/lesson';
import Vue from 'vue';
import { MutationTree } from 'vuex';
import { CHANGE_FAVORITE_IN_ITEM, SET_LESSON_BY_ID, ADD_FAVORITE, DELETE_FAVORITE, LOADED } from './mutationTypes';
import { LessonResponse, UserLessonState } from './types';

export const mutations: MutationTree<UserLessonState> = {
  // [SET_FAVORITE_LESSONS](state, payload: Lesson[]) {
  //   state.favoritelessons = payload;
  // },
  [CHANGE_FAVORITE_IN_ITEM](state, value: boolean) {
    state.lessonByID!.isFavorite! = value;
  },
  [SET_LESSON_BY_ID](state, item: Lesson) {
    state.lessonByID = item;
  },
  [ADD_FAVORITE](state) {
    Vue.set(state.lessonByID!, 'is_favorite', '1');
  },
  [DELETE_FAVORITE](state) {
    Vue.set(state.lessonByID!, 'is_favorite', '0');
  },
  [LOADED](state, data: LessonResponse) {
    state.error = false;
    const { meta, data: Lessons } = data;
    const lessonsAfterLoading: Lesson[] = state.lessons;

    Lessons.forEach(Lesson => {
      const matchIndex = state.lessons.findIndex(item => item.id === Lesson.id);

      if (matchIndex === -1) {
        lessonsAfterLoading.push(Lesson);
      } else {
        lessonsAfterLoading.splice(matchIndex, 1, Lesson);
      }
    });
    state.total = meta.total;
    state.lastPage = meta.last_page;
    state.lessons = lessonsAfterLoading;
  },
};
