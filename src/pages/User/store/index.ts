import { RootState } from '@/store/types';
import Vue from 'vue';
import Vuex, { Module, Store } from 'vuex';
import { actions } from './actions';
import { mutations } from './mutations';
import { UserLessonState } from './types';

Vue.use(Vuex);
export const state: UserLessonState = {
  lessons: [],
  lessonByID: null,
  loading: false,
  error: null,
  filters: {
    page: 1,
  },
  total: 0,
  lastPage: 0,
};

const namespaced: boolean = true;

export const userLesson: Module<UserLessonState, RootState> = {
  namespaced,
  state,
  actions,
  mutations,
};
