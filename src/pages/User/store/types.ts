import { FiltrerAndPagingParams } from '@/filter';
import { Lesson } from '@/types/lesson';

export interface UserLessonState {
  lessons: Lesson[];
  lessonByID: Lesson | null;
  loading: boolean;
  error: any;
  filters: FiltrerAndPagingParams;
  lastPage: number;
  total: number;
}
export interface LessonResponse {
  meta: {
    total: number;
    per_page: number;
    last_page: number;
  };
  data: Lesson[];
}
