import AdminLessonApi from '@/api/AdminLessons';
import { FiltrerAndPagingParams } from '@/filter';
import { RootState } from '@/store/types';
import { ActionTree } from 'vuex';
import {
  ERROR,
  LOADING,
  SET_FAVORITE_LESSONS,
  CHANGE_FAVORITE_IN_ITEM,
  LOADED,
  SET_LESSON_BY_ID,
  ADD_FAVORITE,
  DELETE_FAVORITE,
} from './mutationTypes';

import { UserLessonState } from './types';

export const actions: ActionTree<UserLessonState, RootState> = {
  async getLessonByID({ commit }, id: number | null) {
    try {
      let data;
      if (id === null) {
        data = (await AdminLessonApi.getFirstFreeLesson()).data.data[0];
      } else {
        data = (await AdminLessonApi.getLessonByID(id)).data;
      }
      commit(SET_LESSON_BY_ID, data);
    } catch (error) {
      commit(ERROR, error);
    }
  },
  async getLessons({ commit, state, dispatch }) {
    if (state.filters.page > state.lastPage && state.lastPage !== 0) {
      return;
    }
    try {
      const params: FiltrerAndPagingParams = {
        filters: state.filters.filters,
        page: state.filters.page,
      };
      const { data } = await AdminLessonApi.getLessonsByFilters(params);
      commit(LOADED, data);
    } catch (err) {
      console.error(err);
    }
  },
  async addFavorite({ commit, state }) {
    try {
      await AdminLessonApi.addFavorite(state.lessonByID!.id!);
      commit(ADD_FAVORITE);
    } catch (error) {
      console.error(error);
    }
  },
  async deleteFavorite({ commit, state }) {
    try {
      await AdminLessonApi.deleteFavorite(state.lessonByID!.id!);
      commit(DELETE_FAVORITE);
    } catch (error) {
      console.error(error);
    }
  },

  setFilters({ commit, state }, data: Array<Record<string, string | number>>) {
    state.filters.filters = data;
  },
  setSearch({ commit, state }, value: string) {
    state.filters.search = value;
  },
  setPage({ commit, state }, value: number) {
    state.filters.page = value;
  },
  clearLessons({ state }) {
    state.filters.page = 1;
    state.lastPage = 0;
    state.lessons = [];
  },
  setFavorite({ commit}, value: boolean) {
    commit(CHANGE_FAVORITE_IN_ITEM, value);

  },
};
