import { Component, Vue } from 'vue-property-decorator';
import CustomInput from '@/components/Input';
import CustomButton from '@/components/Button';
import { Validations } from 'vuelidate-property-decorators';
import { minLength, required, sameAs } from 'vuelidate/lib/validators';
import AuthApi from '@/api/auth';

@Component({
  components: {
    CustomInput,
    CustomButton,
  },
})
export default class Password extends Vue {
  @Validations()
  private validations = {
    newPass: {
      password: { required, minLength: minLength(6) },
      confirm: { required, sameAsPassword: sameAs('password') },
    },
  };
  private token: string = '';
  private error: boolean = false;
  private newPass = {
    password: '',
    confirm: '',
  };

  private created() {
    const urlSearchParams = new URLSearchParams(window.location.search);
    this.token = Object.fromEntries(urlSearchParams.entries()).token;
  }

  private async onRestore() {
    this.$v.$touch();
    if (this.$v.$invalid) {
      this.$v.$reset();
      return;
    }
    try {
      const data = {
        passwordRecoveryToken: this.token, // 'Breare <token>'
        password: this.newPass.password,
      };
      await AuthApi.restorePass(data);
      this.$router.push({ name: 'user_login' });
    } catch (error) {
      this.error = true;
    }
  }

  private passwordErrorHandler() {
    if (!this.$v.newPass.password!.$error) {
      if (!this.$v.newPass.password!.required) {
        return 'Обязательное поле';
      }
      if (!this.$v.newPass.password!.minLength) {
        return 'Минимальное количество символов 3';
      }
    }
  }
  private confirmErrorHandler() {
    if (!this.$v.newPass.confirm!.$error) {
      if (!this.$v.newPass.confirm!.required) {
        return 'Обязательное поле';
      }
      if (!this.$v.newPass.confirm!.sameAsPassword) {
        return 'Пароли не совпадают';
      }
    }
  }

  private changeValue(field: string, value: string) {
    // add request
    Vue.set(this.newPass, field, value);
  }
}
