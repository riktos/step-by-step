import { Component, Vue } from 'vue-property-decorator';
import CustomButton from '@/components/Button';
import CustomInput from '@/components/Input';
import { get, set } from '@/utilities/sessionStorage';
import { telegrammKey, userEmailKey, whatsappKey } from '@/constants/sessionStorageKeys';
import AuthApi from '@/api/auth';
import { EditProfileData } from '@/types/auth';
import { Validations } from 'vuelidate-property-decorators';
import { minLength, sameAs, url } from 'vuelidate/lib/validators';

@Component({
  components: {
    CustomButton,
    CustomInput,
  },
})
export default class UserPage extends Vue {
  @Validations()
  private validations = {
    UserData: {
      telegramm: { url },
      whatsapp: { url },
    },
    password: {
      password: { minLength: minLength(6) },
      passwordOld: { minLength: minLength(6) },
      passwordConfirm: { sameAsPassword: sameAs('password') },
    },
  };
  private UserData = {
    telegramm: get(telegrammKey) !== null ? get(telegrammKey) : '',
    whatsapp: get(whatsappKey) !== null ? get(whatsappKey) : '',
  };

  private error = false;
  private password = {
    password: '',
    passwordOld: '',
    passwordConfirm: '',
  };

  private get email() {
    return get(userEmailKey);
  }

  private get isNeedChangePassword() {
    const value = Object.values(this.password).some(passworValue => passworValue !== '');
    return value;
  }

  private changeValue(field: string, value: string) {
    const editObject = Object.keys(this.UserData).includes(field) ? this.UserData : this.password;
    Vue.set(editObject, field, value);
  }
  private onBack() {
    this.$router.go(-1);
  }
  private async onSave() {
    // add api
    const data: EditProfileData = this.isNeedChangePassword
      ? { email: this.email!, ...this.UserData, ...this.password }
      : { email: this.email!, ...this.UserData };
    try {
      await AuthApi.edit(data);
      set(telegrammKey, this.UserData.telegramm!);
      set(whatsappKey, this.UserData.whatsapp!);
      this.onBack();
    } catch (error) {
      this.error = true;
    }
  }

  private validator() {
    if (this.password.password !== this.password.passwordConfirm) {
      return 'Пароли не совпадают';
    }
  }

  private whatsappErrorHandler() {
    if (!this.$v.UserData.whatsapp!.url) {
      return 'Значение не является ссылкой';
    }
  }
  private telegramErrorHandler() {
    if (!this.$v.UserData.telegramm!.url) {
      return 'Значение не является ссылкой';
    }
  }
  private passwordErrorHandler() {
    if (!this.$v.password.password!.$error) {
      if (!this.$v.password.password!.minLength) {
        return 'Минимальное количество символов 6';
      }
    }
  }
  private confirmErrorHandler() {
    if (!this.$v.password.passwordConfirm!.$error) {
      if (!this.$v.password.passwordConfirm!.sameAsPassword) {
        return 'Пароли не совпадают';
      }
    }
  }
  private oldErrorHandler() {
    if (!this.$v.password.passwordOld!.$error) {
      if (!this.$v.password.passwordOld!.minLength) {
        return 'Минимальное количество символов 6';
      }
    }
  }
}
