import { Component, Vue } from 'vue-property-decorator';
import CustomInput from '@/components/Input';
import CustomButton from '@/components/Button';
import { Validations } from 'vuelidate-property-decorators';
import { email, required } from 'vuelidate/lib/validators';
import AuthApi from '@/api/auth';

@Component({
  components: {
    CustomInput,
    CustomButton,
  },
})
export default class Register extends Vue {
  private submit = false;
  private loginError: boolean = false;
  private registerData = {
    email: '',
    login: '',
    password: '',
  };

  @Validations()
  private validations = {
    registerData: {
      email: { required, email },
      login: { required },
      password: { required },
    },
  };

  private isValid(value: string) {
    return value !== '';
  }
  private changeValue(field: string, value: string) {
    if (this.isValid(value)) {
      Vue.set(this.registerData, field, value);
    }
  }
  private async onRegister() {
    this.$v.$touch();
    if (this.$v.$invalid) {
      this.submit = true;
      this.$v.$reset();
      return;
    }
    this.submit = false;

    try {
      await AuthApi.register({
        email: this.registerData.email,
        login: this.registerData.login,
        password: this.registerData.password,
      });
      this.onLogin();
    } catch (error) {
      this.loginError = true;
    }
  }
  private onLogin() {
    this.$router.push({ name: 'user_login' });
  }

  private emailValidation() {
    if (this.submit) {
      if (!this.$v.registerData.email!.required) {
        return 'Обязательное поле';
      }
      if (!this.$v.registerData.email!.email) {
        return 'Email не валидный';
      }
    }
  }
}
