import { Component, Vue } from 'vue-property-decorator';
import CustomInput from '@/components/Input';
import CustomButton from '@/components/Button';
import { email, required } from 'vuelidate/lib/validators';
import { Validations } from 'vuelidate-property-decorators';
import AuthApi from '@/api/auth';

@Component({
  components: {
    CustomInput,
    CustomButton,
  },
})
export default class Password extends Vue {

  @Validations()
  public validations = {
    email: { required, email },
  };
  private isApproved: boolean = false;
  private error: boolean = false;
  private email: string = '';
  private submit = false;
  private newPass = {
    newPass1: '',
    newPass2: '',
  };

  private isValid(value: string) {
    return value !== '';
  }

  private async onRestore() {
    this.$v.$touch();
    if (this.$v.$invalid) {
      this.submit = true;

      return;
    }
    this.submit = false;

    try {
      await AuthApi.sendToEmail(this.email);
      this.onBack();
    } catch (error) {
      console.error((error as any).errors[0].message);
    }
  }

  private changeValue(field: string, value: string) {
    this.error = false;
    this.email = value;
  }

  private onPassword() {
    this.$router.push({ name: 'user_login_form' });
  }

  private emailErrorHandler() {
    if (!this.$v.email!.$error) {
      if (!this.$v.email!.required) {
        return 'Обязательное поле';
      }
      if (!this.$v.email!.email) {
        return 'Неправильная почта';
      }
    }
  }

  private emailValidation() {
    if (this.submit) {
      if (!this.$v.email!.required) { return 'Обязательное поле'; }
      if (!this.$v.email!.email) { return 'Email не валидный'; }
    }
  }

  private onBack() {
    this.$router.push({ name: 'user_login' });
  }
}
