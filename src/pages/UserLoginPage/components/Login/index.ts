import { Component, Vue } from 'vue-property-decorator';
import CustomInput from '@/components/Input';
import CustomButton from '@/components/Button';
import { remove, set } from '@/utilities/sessionStorage';
import {
  authorizationKey,
  authorizationKeyUser,
  telegrammKey,
  userEmailKey,
  whatsappKey,
} from '@/constants/sessionStorageKeys';
import AuthApi from '@/api/auth';
import { Validations } from 'vuelidate-property-decorators';
import { email, required } from 'vuelidate/lib/validators';
import AdminLessonApi from '@/api/AdminLessons';

@Component({
  components: {
    CustomInput,
    CustomButton,
  },
})
export default class Login extends Vue {
  private loginError = false;

  private submited = false;
  private loginData = {
    email: '',
    password: '',
  };

  @Validations()
  private validations = {
    loginData: {
      email: { required, email },
      password: { required },
    },
  };
  private changeValue(field: string, value: string) {
    Vue.set(this.loginData, field, value);
  }

  private async onLogin() {
    this.$v.$touch();
    if (this.$v.$invalid) {
      this.submited = true;
      this.$v.$reset();
      return;
    }
    this.submited = false;

    // if (!!this.validator('email') || !!this.validator('pass')) {
    // return;
    // } else {
    try {
      const { data, status } = await AuthApi.authorize({
        email: this.loginData.email,
        password: this.loginData.password,
      });

      if (status === 200) {
        set(authorizationKeyUser, data.token);
        set(telegrammKey, data.user.telegramm);
        set(whatsappKey, data.user.whatsapp);
        set(userEmailKey, data.user.email);
        remove(authorizationKey);
      }
      const freeLessonId = (await AdminLessonApi.getFirstFreeLesson()).data.data[0].id;
      this.$router.push({
        name: 'user_lesson',
        params: {
          id: freeLessonId,
        },
      });
    } catch (error) {
      this.loginError = true;
    }
  }
  private onRegister() {
    this.$router.push({ name: 'user_registration_form' });
  }
  private onPassword() {
    this.$router.push({ name: 'user_change_password_form' });
  }

  private emailValidation() {
    if (this.submited) {
      if (!this.$v.loginData.email!.required) {
        return 'Обязательное поле';
      }
      if (!this.$v.loginData.email!.email) {
        return 'Email не валидный';
      }
    }
  }
}
