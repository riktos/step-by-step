export const get = (key: string): string | null => window.sessionStorage.getItem(key);

export const set = (key: string, value: string): void => window.sessionStorage.setItem(key, value);

export const remove = (key: string): void => window.sessionStorage.removeItem(key);
