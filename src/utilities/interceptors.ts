import { authorizationKey, authorizationKeyUser } from '@/constants/sessionStorageKeys';
import { AxiosRequestConfig, AxiosResponse } from 'axios';
import toastr from 'toastr';
import { get } from './sessionStorage';

export const onRequestSuccess = (config: AxiosRequestConfig): AxiosRequestConfig => {
  const authorization = get(authorizationKey);
  const authorizationUser = get(authorizationKeyUser);
  if (authorization !== null) {
    config.headers.Authorization = `Bearer ${authorization}`;
  }
  if (authorizationUser !== null) {
    config.headers.Authorization = `Bearer ${authorizationUser}`;
  }

  return config;
};

export const onRequestError = (error: any): Promise<any> => Promise.reject(error);

export const onResposnseSuccess = (response: AxiosResponse): AxiosResponse => response;

export const onResponseError = (error: any, options: any): Promise<any> | undefined => {
  const {
    response,
    config: { params: { forbiddenNoLogin = false } = {} },
  } = error;

  const { method, params } = response.config;
  const { data, status } = response;

  if (!response) {
    toastr.error('Server Unavailable. Try again later.');
    return Promise.reject(null);
  }

  let message = '';
  if (params && params.disableError) {
    return;
  } else {
    message = data.Message || data || '';
  }

  if (method === 'post') {
    if (!params) {
      toastr.error(message);
    }
  }
  if ([403, 403].includes(status) && !forbiddenNoLogin) {
    return Promise.reject(message);
  }

  if (status === 500) {
    if (params && params.disableError) {
      return;
    } else {
      toastr.error(message);
    }

    return Promise.reject(message);
  } else {
    toastr.error(message);
  }
  return Promise.reject(message);
};
