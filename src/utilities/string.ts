export function YouTubeGetID(url: string) {
  const url2 = url.split(/(vi\/|v=|\/v\/|youtu\.be\/|\/embed\/)/);
  return url2[2] !== undefined ? url2[2].split(/[^0-9a-z_\-]/i)[0] : url2[0];
}
export const validateEmail = (email: string) => {
  return email.match(
    /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
  );
};
