import config from '@/config';
import axios, { AxiosInstance } from 'axios';
import { onRequestError, onRequestSuccess, onResponseError, onResposnseSuccess } from '@/utilities/interceptors';

const defaultOptions: any = {
  baseURL: 'https://bystep.io/api/',
  withCredentials: true,
};

export const createAxiosInstance = (options: any = defaultOptions): AxiosInstance => {
  const axiosInstance = axios.create(options);
  axiosInstance.interceptors.request.use(onRequestSuccess, onRequestError);
  axiosInstance.interceptors.response.use(onResposnseSuccess, error => onResponseError(error, options));
  return axiosInstance;
};

const axiosBase = createAxiosInstance();

export default axiosBase;
