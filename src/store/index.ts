import { adminLesson } from '@/pages/Admin/store';
import { userLesson } from '@/pages/User/store';
import Vue from 'vue';
import Vuex, { Store } from 'vuex';
import { RootState } from './types';

Vue.use(Vuex);

const store = new Vuex.Store<RootState>({
  modules: { adminLesson, userLesson },
});

export default store;
