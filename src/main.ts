import Vue from 'vue';
import App from './App.vue';
import router from './router';
import toastr from 'toastr';
import './assets/styles/main.scss';
import store from './store';
import axios from './utilities/axios';
import Vuelidate from 'vuelidate';
import InfiniteScroll from './directive';

axios.defaults.headers.common['Content-Type'] = 'application/x-www-form-urlencoded';
axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*';

Vue.use(Vuelidate);

Vue.directive('infinite-scroll', InfiniteScroll);

export const vmInst = new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');

toastr.options = {
  timeOut: 0,
  extendedTimeOut: 0,
  tapToDismiss: false,
  debug: false,
  positionClass: 'toast-top-right',
  closeButton: true,
  closeDuration: 0,
};
