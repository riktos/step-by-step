export interface FiltrerAndPagingParams {
  filters?: Array<Record<string, string | number>>;
  search?: string;
  page: number;
}

export const parsedRequestParams = (payload: FiltrerAndPagingParams) => {
  const { filters, search, page } = payload;
  const requestString = [];

  const pagingAsString = `?paging[page]=${page}&paging[limit]=6`;
  requestString.push(pagingAsString);

  if (filters) {
    const filtersAsString: string = filters
      .map(filter => {
        let parsedFilter;

        for (const field in filter) {
          if (Object.prototype.hasOwnProperty.call(filter, field)) {
            parsedFilter = `filter[${field}]=${filter[field]}`;
          }
        }
        return parsedFilter;
      })
      .join('&');
    requestString.push(filtersAsString);
  }
  if (search && search !== '') {
    const searchAsString = `filter[name][operator]=like&filter[name][value]=${search}`;
    requestString.push(searchAsString);
  }
  return requestString.join('&');
};
