import Vue from 'vue';
import Router from 'vue-router';
import Login from './layouts/Login';
import AdminPage from '@/pages/Admin';
import DefaultLayout from './layouts/MainLayout';
import LessonPage from './pages/Lesson';
import Profile from './pages/Profile';
import UserPage from './pages/User';
import UserLayout from './layouts/User';
import UserLoginPage from '@/pages/UserLoginPage';
import AdminLoginPage from '@/pages/AdminLoginPage';

import UserLoginForm from '@/pages/UserLoginPage/components/Login';
import UserPasswordForm from '@/pages/UserLoginPage/components/Password';
import UserRegistrationForm from '@/pages/UserLoginPage/components/Register';

import { get } from './utilities/sessionStorage';
import { authorizationKey, authorizationKeyUser } from './constants/sessionStorageKeys';
import ResetPassword from './pages/ResetPassword';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '',
      name: 'login',
      component: Login,
      redirect: { name: 'user_login' },
      children: [
        {
          path: '/admin_login',
          name: 'admin_login',
          component: AdminLoginPage,
        },
        {
          path: '/user_login',
          name: 'user_login',
          component: UserLoginPage,
          redirect: { name: 'user_login_form' },
          children: [
            {
              path: '',
              name: 'user_login_form',
              component: UserLoginForm,
            },
            {
              path: '/user_login/change_password',
              name: 'user_change_password_form',
              component: UserPasswordForm,
            }, {
              path: '/password-recovery',
              name: 'reset',
              component: ResetPassword,
            },
            {
              path: '/user_login/registration',
              name: 'user_registration_form',
              component: UserRegistrationForm,
            },
          ],
        },
      ],
    },
    {
      path: '/admin',
      component: DefaultLayout,
      children: [
        {
          name: 'admin',
          path: '',
          component: AdminPage,
        },
        {
          path: '/admin/edit/:id',
          name: 'edit',
          component: LessonPage,
        },
        {
          path: '/admin/create',
          name: 'create',
          component: LessonPage,
        },
      ],
    },
    {
      path: '/user',
      name: 'home',
      component: UserLayout,
      children: [
        {
          name: 'user_lesson',
          path: '/user/:id',
          component: UserPage,
        },
        {
          path: '/user_profile',
          name: 'user_profile',
          component: Profile,
        },
      ],
    },
  ],
});

const UserRoutes = ['user_profile', 'user'];
const AdminRoutes = ['admin', 'create'];

// router.beforeEach((to, from, next) => {
//   if (get(authorizationKey) === null && !window.location.href.match(/login/gi)) {
//     window.location.href = '/login';
//   }

//   next();
// });

router.onError(error => {
  if (/loading chunk \d\w* failed./i.test(error.message) && navigator.onLine) {
    window.location.reload();
  }
});

export default router;
