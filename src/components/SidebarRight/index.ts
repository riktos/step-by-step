import { userEmailKey } from '@/constants/sessionStorageKeys';
import { FiltrerAndPagingParams } from '@/filter';
import { Lesson } from '@/types/lesson';
import { get } from '@/utilities/sessionStorage';
import { YouTubeGetID } from '@/utilities/string';
import { Component, Vue, Watch } from 'vue-property-decorator';
import { namespace } from 'vuex-class';
import Loader from '@/components/Loader';

const adminLesson = namespace('adminLesson');
const userLesson = namespace('userLesson');
@Component({
  components: {
    Loader,
  },
})
export default class Sidebar extends Vue {
  private get name() {
    return get(userEmailKey)!.replace(/@.+/gi, '');
  }

  private get items() {
    return this.lessons;
  }
  private get page() {
    return this.filters.page;
  }
  private get pagefilters() {
    return this.filters.filters;
  }

  private get currentCategory(): string {
    return this.options.find(item => item.active === true)!.key;
  }
  private firstLetter = this.name.charAt(0).toUpperCase();
  private activeLessonsArr: string = 'beginner';
  private isFavor: boolean = false;
  private loading: boolean = false;

  @userLesson.State private lessons!: Lesson[];
  @userLesson.State private filters!: FiltrerAndPagingParams;
  @userLesson.State private lessonByID!: Lesson;

  @userLesson.Action private getLessons!: () => Promise<void>;
  @userLesson.Action private setFilters!: (data: Array<Record<string, string | number>>) => void;
  @userLesson.Action private setPage!: (value: number) => void;
  @userLesson.Action private clearLessons!: () => void;

  private componentFilters: Array<Record<string, any>> = [];

  private options = [
    {
      active: true,
      key: 'beginner',
      value: 'Начальный',
    },
    {
      active: false,
      key: 'pro',
      value: 'Продвинутый',
    },
  ];

  private async created() {
    const categoryFilter = { category: this.currentCategory };
    // const favoriteFilter = { is_favorite: this.isFavor ? '1' : '0' };
    this.componentFilters = [categoryFilter];
    this.setFilters(this.componentFilters);
    this.setPage(1);
    this.loading = true;
    try {
      await this.getLessons();
    } finally {
      this.loading = false;
    }
  }

  private get IsFavoriteOpenedLesson() {
    if (this.lessonByID) {
      return this.lessonByID.isFavorite!;
    }
    return false;
  }

  @Watch('IsFavoriteOpenedLesson')
  private async onChangeFavorite() {
    this.loading = true;
    try {
      await this.getLessons();
    } finally {
      this.loading = false;
    }
  }

  @Watch('currentCategory')
  @Watch('isFavor')
  private onChangeFilter() {
    const categoryFilter = { category: this.currentCategory };
    const favoriteFilter = { is_favorite: this.isFavor };
    this.componentFilters = [categoryFilter];
    if (this.isFavor) {
      this.componentFilters.push(favoriteFilter);
    }
    this.setFilters(this.componentFilters);
  }

  private async loadNext() {
    console.log(123);
    this.loading = true;
    try {
      this.setPage(this.page + 1);

      await this.getLessons();
    } finally {
      this.loading = false;
    }
  }

  @Watch('loading')
  private temp(value: boolean) {
    console.log(value);
  }
  @Watch('currentCategory')
  @Watch('isFavor')
  private async onFilters() {
    this.clearLessons();
    this.setPage(1);
    this.loading = true;
    try {
      await this.getLessons();
    } finally {
      this.loading = false;
    }
  }

  private onToggle(value: string) {
    if (this.activeLessonsArr !== value) {
      const lastLessonArr = this.options.findIndex(({ key }) => key === this.activeLessonsArr);
      const newLessonArr = this.options.findIndex(({ key }) => key === value);
      Vue.set(this.options[lastLessonArr], 'active', false);
      Vue.set(this.options[newLessonArr], 'active', true);
      this.activeLessonsArr = value;
    }
  }

  private toggleFavor() {
    this.isFavor = !this.isFavor;
  }

  private imageForLesson(link: string): string {
    const videoID = YouTubeGetID(link);
    return `https://i3.ytimg.com/vi/${videoID}/maxresdefault.jpg`;
  }

  private onClick(id: number) {
    this.$router.push({
      name: 'user_lesson',
      params: {
        id: id.toString(),
      },
    });
  }
}
