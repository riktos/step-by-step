import { Component, Prop, Vue } from 'vue-property-decorator';
const ClickOutside = require('vue-click-outside');
@Component({
  directives: {
    ClickOutside,
  },
})
export default class Button extends Vue {
  @Prop(Array) private options!: Array<Record<string, any>>;
  private debounce: any = null;
  private selected: Record<string, any> = this.options[0];
  private open = false;

  private onClick(value: Record<string, any>) {
    this.selected = value;
    this.$emit('onClick', value.key);
    this.hide();
  }
  private hide() {
    this.open = false;
  }
}
