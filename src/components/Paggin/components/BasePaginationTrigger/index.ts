import { Lesson } from '@/types/lesson';
import { YouTubeGetID } from '@/utilities/string';
import { Component, Prop, Vue } from 'vue-property-decorator';
import { namespace } from 'vuex-class';

const adminLesson = namespace('adminLesson');

@Component
export default class BaseButton extends Vue {
  @Prop(Number) private pageNumber!: number;

  private onClick() {
    this.$emit('loadPage', this.pageNumber);
  }
}
