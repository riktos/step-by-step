import { uniq } from '@/helper';
import { Lesson } from '@/types/lesson';
import { YouTubeGetID } from '@/utilities/string';
import { Component, Prop, Vue } from 'vue-property-decorator';
import { namespace } from 'vuex-class';
import BaseButton from './components/BaseButton';
import BasePaginationTrigger from './components/BasePaginationTrigger';

const adminLesson = namespace('adminLesson');

@Component({
  components: {
    BaseButton,
    BasePaginationTrigger,
  },
})
export default class BasePagin extends Vue {
  @Prop({ type: Number, required: true }) private currentPage!: number;
  @Prop({ type: Number, required: true }) private pageCount!: number;
  // @Prop({ type: Number, required: true }) private pageOnNumber!: number;
  @Prop({ type: Number }) private visiblePagesCount!: number;

  get isPreviousButtonDisabled() {
    return this.currentPage === 1;
  }

  get isNextButtonDisabled() {
    return this.currentPage === this.pageCount || this.pageCount === 0;
  }

  get paginationTriggers() {
    const currentPage = this.currentPage;
    const pageCount = this.pageCount === 0 ? 1 : this.pageCount;
    const visiblePagesCount = this.visiblePagesCount;
    const visiblePagesThreshold = (visiblePagesCount - 1) / 2;
    const pagintationTriggersArray = Array(
      this.visiblePagesCount >= pageCount ? pageCount - 1 : this.visiblePagesCount - 1,
    ).fill(0);

    if (currentPage <= visiblePagesThreshold + 1) {
      pagintationTriggersArray[0] = 1;
      const pagintationTriggers = pagintationTriggersArray.map((paginationTrigger, index) => {
        return pagintationTriggersArray[0] + index;
      });

      pagintationTriggers.push(pageCount);

      return pagintationTriggers.filter(uniq);
    }

    if (currentPage >= pageCount - visiblePagesThreshold + 1) {
      const pagintationTriggers = pagintationTriggersArray.map((paginationTrigger, index) => {
        return pageCount - index;
      });

      pagintationTriggers.reverse().unshift(1);

      return pagintationTriggers.filter(uniq);
    }

    pagintationTriggersArray[0] = currentPage - visiblePagesThreshold + 1;
    const pagintationTriggers = pagintationTriggersArray.map((paginationTrigger, index) => {
      return pagintationTriggersArray[0] + index;
    });

    pagintationTriggers.unshift(1);
    pagintationTriggers[pagintationTriggers.length - 1] = pageCount;

    return pagintationTriggers.filter(uniq);
  }

  private nextPage() {
    this.$emit('nextPage');
  }

  private onLoadPage(value: any) {
    this.$emit('loadPage', value);
  }

  private previousPage() {
    this.$emit('previousPage');
  }
}
