import { Component, Prop, Vue } from 'vue-property-decorator';

@Component
export default class Button extends Vue {
  @Prop(String) public value!: string;
  @Prop(String) public type?: string;

  private onClick() {
    this.$emit('onClick');
  }
}
