import { Component, Vue } from 'vue-property-decorator';

interface NavigationItem {
  key: string;
  label: string;
  isActive: boolean;
}

@Component
export default class Header extends Vue {

}
