import { Component, Prop, Vue } from 'vue-property-decorator';

@Component
export default class Input extends Vue {
  @Prop(String) private value?: string;
  @Prop(String) private label?: string;
  @Prop(String) private placeholder?: string;
  @Prop(String) private type?: string;
  @Prop(String) private isValidation?: string;
  @Prop(String) private width?: string;
  private validation =  true;
  private innerText: string = this.value || '';
  private onInput(event: string) {
    this.$emit('onInput', this.innerText);
    this.$emit('clearValidation', true);
  }

  get styles() {
    return {
      width: this.width || '100%',
    };
  }
  private destroed() {
    this.innerText = '';
  }
}
