import { Component, Vue } from 'vue-property-decorator';

@Component
export default class Sidebar extends Vue {
  get links() {
    if (this.$route.path.match('admin')) {
      return [{ name: 'Управление уроками', active: true }];
    } else {
      return [
        { name: 'Уроки', active: true },
        { name: 'Как купить урок', active: false },
      ];
    }
  }
}
