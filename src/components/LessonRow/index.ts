import { Lesson } from '@/types/lesson';
import { YouTubeGetID } from '@/utilities/string';
import { Component, Prop, Vue } from 'vue-property-decorator';
import { namespace } from 'vuex-class';

const adminLesson = namespace('adminLesson');

@Component
export default class LessonRow extends Vue {
  @Prop(Object) private item!: Lesson;

  @adminLesson.Action private deleteLesson!: (id: number) => Promise<void>;

  private async onDelete() {
    try {
      await this.deleteLesson(this.item.id!);
    } catch (error) {
      console.error(error);
    }
  }
  private onEdit() {
    this.$router.push({
      name: 'edit',
      params: {
        id: this.item.id!.toString(),
      },
    });
  }
  private imageForLesson(): string {
    const videoID = YouTubeGetID(this.item.youtubeCode);
    return `http://i3.ytimg.com/vi/${videoID}/hqdefault.jpg`;
  }
}
