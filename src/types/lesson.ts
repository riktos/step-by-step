export interface Lesson {
  id: number | null;
  category: string;
  name: string;
  description: string;
  fullDescription: string;
  author: string;
  telegram: string;
  instagram: string;
  youtube: string;
  youtubeCode: string;
  price: number;
  is_favorite: boolean;
  isFavorite?: boolean;
}

export enum CategoryEnum {
  beginner = 'Начинающий',
  pro = 'Продвинутый',
}
