export interface EditProfileData {
  email: string;
  password?: string;
  passwordOld?: string;
  passwordConfirm?: string;
  whatsapp: string | null;
  telegramm: string | null;
}
